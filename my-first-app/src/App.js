import React, { Component } from 'react';
import './App.scss'
import HelloWorld from './components/HelloWorld/HelloWorld';
import Components from './components/Components';
import ComponentTest from './components/ComponentTest';
import { BrowserRouter as Router, Route, NavLink, Switch  } from "react-router-dom";
import CarDetalesTemplate from './components/LearnReact/Car/CarDetalesTemplate'
import Error404 from './components/404/404'

/*const menu = [
  {
    link: '/articles',
    label: 'Articles'
  },
  {
    link: '/contacts',
    label: 'Contacts'
  },
  {
    link: '/posts',
    label: 'Posts'
  }
];*/

class App extends Component {

  state = {
    activePage: true
  }

  render(){
    return (
      <Router>
        <div className='container'>
          <nav className='nav'>
            <ul className='nav__items'>
              <li className='nav__item'>
                <NavLink to="/" exact className='nav__link' activeClassName='nav__link-active'>Home</NavLink>
              </li>
              <li className='nav__item'>
                <NavLink to="/components/" className='nav__link' activeClassName='nav__link-active'>Components</NavLink>
              </li>
              <li className='nav__item'>
                <NavLink  to="/componenttest/" className='nav__link' activeClassName='nav__link-active'>ComponentTest</NavLink>
              </li>
            </ul>
            <hr />
            <div style={{textAlign: 'center'}}>
              <button onClick={() => (this.setState({activePage:!this.state.activePage}))}>{this.state.activePage ? 'Do disactive compontents page' : 'Do active compontents page'}</button>
            </div>
          </nav>
  
          <Switch>
            <Route path="/" exact component={HelloWorld} />
            {this.state.activePage ? <Route path="/components/" component={Components} /> : null}
            
            <Route path="/componenttest/:name"  component={CarDetalesTemplate} />
            <Route path="/componenttest/"  component={ComponentTest} />
            <Route component={Error404} />
          </Switch>
          
        </div>
      </Router>
    )}
}

export default App;
