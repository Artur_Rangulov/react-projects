import React, { Component } from 'react';
import './style.scss';

function BoilingVerdict(props){
   const water = {name:'Вода', temp:100};
   
    if (+props.value >= water.temp){
     return(
       <div>
         <p>{water.name} закипит</p>
       </div>
       );
    }else{
     return (
       <div>
         <p>{water.name} не закипит</p>
       </div>
     )
   }
}

function toCelsius(fahrenheit) {
    return (fahrenheit - 32) * 5 / 9;
}
  
function toFahrenheit(celsius) {
    return (celsius * 9 / 5) + 32;
}

function tryConvert(temperature, convert) {
    const input = parseFloat(temperature);
    if (Number.isNaN(input)){
        return ''
    };
    const output = convert(input);
    const rounded = Math.round(output * 1000) / 1000;
    return rounded.toString();

}
 
const scaleNames = {
   c: 'Цельсия',
   f: 'Фаренгейта'
 };
 
class TemperatureInput extends Component {

    handle = (event) => {
        this.props.onTemperatureChange(event.target.value)
    }
    render() {
        const temp = this.props.temperature;
        const scale = this.props.scale;
        return (
            <div>
                <label>
                    Введите температуру в градусах {scaleNames[scale]} <input type="number" onChange={this.handle} value={temp}/>
                </label>

            </div>
        );
    }
}
 
class TempCalculator extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            scale: 's',
            temperature: ''
        }
    }


    handleCelsiusChange = (temperature) => {
        this.setState({scale: 'c', temperature: temperature});
    }
    
    handleFahrenheitChange = (temperature) => {
        this.setState({scale: 'f', temperature: temperature});
    }

    render() {
        const scale = this.state.scale;
        const temperature = this.state.temperature;
        const celsius = scale === 'f' ? tryConvert(temperature, toCelsius) : temperature;
        const fahrenheit = scale === 'c' ? tryConvert(temperature, toFahrenheit) : temperature;

        return (
            <div className='tempCalculator'>
                <h1 className='h1'>Boiling point calculator</h1>
                <TemperatureInput
                    scale="c"
                    temperature={celsius}
                    onTemperatureChange={this.handleCelsiusChange} />

                <TemperatureInput
                  scale="f"
                  temperature={fahrenheit}
                  onTemperatureChange={this.handleFahrenheitChange} />
                <BoilingVerdict 
                    value={parseFloat(celsius)}/>
            </div>
        );
    }
}
 
export default TempCalculator;