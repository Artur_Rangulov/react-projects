import React, { Component } from 'react';

class HelloWorld extends Component{
    render(){
        return(
            <div>
                <h2>Home</h2>
                <p style={{color:"#555", fontWeight:"bold", textTransform:"uppercase"}}>Hello World!</p>
            </div>
        )
    }
}

export default HelloWorld;