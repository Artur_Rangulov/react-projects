import React, { Component } from 'react';

class Car extends Component{
    render(props){
        const inputClasses = ['cars__input'];

        if (this.props.name !== ''){
            inputClasses.push('cars__input-green')
        }else{
            inputClasses.push('cars__input-red')
        }

        if (this.props.name.length > 4){
            inputClasses.push('cars__input-green2x')
        }

        return(
            <div className='cars__item'>
                <h3>Car name: {this.props.name}</h3>
                <p>Year: {this.props.year}</p>
                <label>Change Name of Car   
                    <input 
                        className={inputClasses.join(' ')}
                        type='text'
                        onChange={this.props.changeNameOfCar}
                        value={this.props.name}/>
                </label>
                <div><button onClick={this.props.delete}>Delete car</button></div>
            </div>
        )
    }
}

export default Car;