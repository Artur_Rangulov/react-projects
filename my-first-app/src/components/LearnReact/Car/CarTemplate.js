import React from 'react'
import {withRouter} from 'react-router-dom'

const CarTemplate = props => {
    console.log(props)
    return(
        <div 
            style={{cursor: 'pointer',border: '1px solid black', borderRadius:'5px', padding:'10px', width: '400px', margin: '15px auto'}}
            onClick={() => props.history.push('/componenttest/' + props.car.name.toLowerCase())}
            >
            <h3>Name: {props.car.name}</h3>
            <p>Year: {props.car.year}</p>
        </div>
    )
}

export default withRouter(CarTemplate);