import React, {Component} from 'react'
import CarTemplate from './CarTemplate'

export default class CarsTemplate extends Component{

    state = {
        cars: [
            {name: 'Ford', year: 2012},
            {name: 'Mazda', year: 2018}
        ],
    }

    render(){
        return(
            <div>
                {this.state.cars.map( (car, index) => {
                    return <CarTemplate car={car} key={index} />
                }

                )}
            </div>
        )
    }
}
