import React, { Component } from 'react';
import Car from './Car';
import Counter from '../Counter/Counter';
import './style.scss';

class Cars extends Component{
    state = {
        cars: [
            {name: 'Ford', year: 2012},
            {name: 'Mazda', year: 2018}
        ],
        newCar: {name: '', year: ''},
        pageTitle: 'List of cars',
        showCars: false
    }

    // changeTitleHandler = (pageTitle) => {
    //     this.setState({
    //         pageTitle: pageTitle
    //     })
    // }

    // handleInput = (event) => {
    //     this.setState({
    //         pageTitle: event.target.value
    //     })
    // }

    toggleCarsHandler = () => {
        this.setState({
            showCars: !this.state.showCars
        })
    }

    onChangeName = (newName, index) => {
        const car = this.state.cars[index];
        car.name = newName;
        const cars = [...this.state.cars]
        cars[index] = car;
        this.setState({
            cars: cars
        })
    }

    deleteHandler (index){
        const cars = this.state.cars.concat();
        cars.splice(index, 1);
        this.setState({
            cars: cars
        })
    }

    addNewCar = (event) => {
        const newCar = this.state.newCar;
        if (event.target.name === 'NameOfNewCar') {newCar.name = event.target.value};
        if (event.target.name === 'YearOfNewCar') {newCar.year = event.target.value};
        this.setState({
            newCar: newCar
        })
    }

    addCarHandler = (event) => {
        event.preventDefault();
        const cars = this.state.cars.concat();
        if (this.state.newCar.name !== '' && this.state.newCar.year !== '') {
            cars.push(this.state.newCar)
            this.setState({
                cars: cars,
                newCar: {name: '', year: ''}
            })
        }

    }

    render(){
        let cars = null;
        if (this.state.showCars){
            cars = this.state.cars.map((car, index) => {
                return(
                    <Car 
                        key={index} 
                        name={car.name} 
                        year={car.year} 
                        changeNameOfCar={(event) => this.onChangeName(event.target.value, index)}
                        delete = {this.deleteHandler.bind(this, index)}
                        />
                )
            })
        }

        return(
           
            <div className='cars'>
                <h1 className='h1'>{this.state.pageTitle}</h1>
                <Counter />
                {/* <input type='text' onChange={this.handleInput} /> */}
                <form className='cars__form' onSubmit={this.addCarHandler}>
                    <label>Car name <input type='text' name="NameOfNewCar" onChange={this.addNewCar} value={this.state.newCar.name}/> </label>
                    <label>Car name <input type='text' name="YearOfNewCar" onChange={this.addNewCar} value={this.state.newCar.year}/> </label>
                    <button>Add new car</button>
                </form>
                <button className='cars__button' onClick={this.toggleCarsHandler}>Toggle all cars</button>
                <div className='cars__items'>
                    { cars }
                </div>
                {/* <Car 
                    name={cars[0].name} 
                    year={'2018'} 
                    ChangeMainTitle={this.changeTitleHandler.bind(this, cars[0].name)}/>  //для передачи метода используется bind
                <Car 
                    name={cars[1].name} 
                    year={'2012'}
                    ChangeMainTitle={() => (this.changeTitleHandler(cars[1].name))}/> */  //дл передачи метода используется анонимная функция
                }  
            </div>
        )
    }
}

export default Cars;