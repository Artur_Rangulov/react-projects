import React, {Component} from 'react'

class Counter extends Component{
    constructor(props){
        super(props)
        this.state = {
            count: 0
        }
    }

    handlePlus = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    handleMinus = () => {
        this.setState({
            count: this.state.count - 1
        })
    }

    render(){
        return[
                <h2 key={'1'}>Counter</h2>,
                <button key={'2'} onClick={this.handleMinus}>-</button>,
                <span key={'3'} > {this.state.count} </span>,
                <button key={'4'} onClick={() => this.setState({count: this.state.count +1})}>+</button>
        ]
    }
}

export default Counter;