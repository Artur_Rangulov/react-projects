import React, { Component } from 'react';
import './style.scss';
//import Chart from './components/Chart/Chart';

class Header extends Component {

  
    render() {
    return (
      <div className='Header'> 
        {this.props.items.map((item,index) => 
            <a className="header__link" href={item.link} key={index}>{item.label}</a>
        )}
      </div>
    )
  }
}

export default Header;