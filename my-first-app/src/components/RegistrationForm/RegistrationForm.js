import React, { Component } from 'react';
import './style.scss';


class RegistrationForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            sendEmail: ''
        }
        this.handleEmailChange = this.handleEmailChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleSubmit(event){
        event.preventDefault();
        console.log('form is sumbitted', this.state.email)
        this.setState({
            sendEmail: this.state.email
        })
    }

    handleEmailChange(event){
        console.log('email was change', event.target.value)
        this.setState({
            email: event.target.value
        })
    }



    render() {
        return (
            <div className='RegistrationForm'>
                <h2>RegistrationForm (работа с input)</h2>
                <form onSubmit={this.handleSubmit}>
                    <input 
                        type='text' 
                        placeholder='E-mail' 
                        value={this.state.email} 
                        onChange={this.handleEmailChange} />
                        <button>Send</button>
                </form>
                <p>Your mail is: {this.state.email}</p>
                <p>You send mail is: {this.state.sendEmail}</p>
        </div>
    )};
}

export default RegistrationForm;
