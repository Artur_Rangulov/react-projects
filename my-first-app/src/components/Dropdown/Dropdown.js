import React, { Component } from 'react';
import './style.css';

class Dropdown extends Component {
	constructor(props){
		super(props);
		this.state = {isOpened: false}
    }
	
	toggleState (){
		this.setState({isOpened: !this.state.isOpened})
	}
	
	render() {
		let dropdownText;
		if (this.state.isOpened){
			dropdownText = this.props.dropdownText
		}

		console.log(this.state);
    return (
      <div className="dropdown" onClick={this.toggleState.bind(this)}> 
            <p>{this.props.name}</p>
						<p>{dropdownText}</p>
      </div>
    )
  }
}

export default Dropdown;