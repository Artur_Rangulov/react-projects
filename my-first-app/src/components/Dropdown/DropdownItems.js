import React, { Component } from 'react';
import Dropdown from './Dropdown'
import './style.css';

class DropdownItems extends Component {
	render() {
        return (
            <div>
                <h2>Dropdown (работа с раскрывающимися блоками)</h2>
                <Dropdown name='open me' dropdownText='Yep-yep' />
                <Dropdown name='open me 2' dropdownText='Yep-yep 2' />
            </div>
        )
    }
}

export default DropdownItems;