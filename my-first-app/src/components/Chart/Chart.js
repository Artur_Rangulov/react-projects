import React, { Component } from 'react';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import Select from '../Select/Select'
import options from './options'




class Chart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: options
        };
    }
    render(){
        return (
            <div>
                <h2>Chart</h2>
                <div className='ChartDraw'>
                    <HighchartsReact highcharts={Highcharts} options={this.state.options}/>
                </div>
                <div>
                    <Select />
                </div>
            </div>
        );
    }
}

export default Chart;
