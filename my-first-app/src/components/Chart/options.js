import products from './data'


const options = {   //настройки Highcharts
    chart: {
        type: 'scatter',
        zoomType: 'xy'
    },
    title: {
        text: 'Goods'
    },
    xAxis: {
        title: {
            enabled: true,
            text: 'Feature 1'
        },
        startOnTick: true,
        endOnTick: true,
        showLastLabel: true,
        gridLineWidth: 1
    },
    yAxis: {
        title: {
            text: 'Feature 2'
        },
        gridLineWidth: 1
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
    },
    plotOptions: {
        scatter: {
            marker: {
                radius: 3,
                states: {
                    hover: {
                        enabled: true,
                        lineColor: 'rgb(100,100,100)'
                    }
                }
            },
            states: {
                hover: {
                    marker: {
                        enabled: false
                    }
                }
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x}, {point.y}'
            }
        }
    },
    series: products  // подгружаем данные из data.js
    };

export default options;