import React, { Component } from 'react';
import './style.scss'


class ClothesTable extends Component{
  
    constructor(props){
        super(props);
        this.state = {
          filterText: '',
          isStockedOnly: false 
        }
      }
      
      handleInStockChange = (isStockedOnly) => {
        this.setState({
          isStockedOnly: isStockedOnly
        })
      }
      
      handleFilterTextChange = (text) => {
        this.setState({
          filterText: text
        })
      }
      
      render(){
        return(
          <div className='clothesTables__item'>
            <h3>{this.props.title}</h3>
            <SearchBar 
              isStockedOnly={this.state.isStockedOnly} 
              filterText={this.state.filterText} 
              onFilterTextChange={this.handleFilterTextChange}
              onInStockChange={this.handleInStockChange}/>
            <ProductTable 
              products={this.props.products} 
              isStockedOnly={this.state.isStockedOnly}
              filterText={this.state.filterText} 
              onFilterTextChange={this.handleFilterTextChange}/>
          </div>
        )
      }
}
class SearchBar extends React.Component{
  
    handleFilterTextChange = (event) => {
      this.props.onFilterTextChange(event.target.value)
    }
    
    handleInStockChange = (event) => {
      this.props.onInStockChange(event.target.checked)
    }
    
   
    render(){
      return(
        <form>
          <p><input type='text' placeholder='Search' value={this.props.filterText} onChange={this.handleFilterTextChange}/></p>
          <p><input type='checkbox' checked={this.props.isStockedOnly} onChange={this.handleInStockChange}/>Only show products in stock</p>
        </form>
      )
    }
  }
  
  class ProductTable extends React.Component{
    render(){
      const rows = [];
      const isStockedOnly = this.props.isStockedOnly;
      let lastCategory = null;
      
      
      this.props.products.forEach((product) => {    
        if (product.name.indexOf(this.props.filterText) === -1){
          return
        }
        if (isStockedOnly && !product.stocked){
          return
        }
        if (product.category !== lastCategory){
          rows.push(
          <ProductCategoryRow 
              category={product.category}
              key={product.category}
            />
          )
        }
        
        rows.push(
          <ProductRow 
              product={product}
              key={product.name}
            />
          )
        lastCategory = product.category;
      })
      
      return(
        <table className='clothesTables__table'>
          <thead>
            <tr>
              <th>Name</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>{rows}</tbody>
        </table>
      )
    }
  }
  
  class ProductCategoryRow extends React.Component{
    render(){
      return(
        <tr className='clothesTables__table_category'>
          <td colSpan="2"> {this.props.category} </td>
        </tr>
      )
    }
  }
  
  class ProductRow extends React.Component{
    
    render(){
      const product = this.props.product;
      const name = product.stocked ? 
            product.name : 
            <span style={{color:'red'}}>
                {product.name}
            </span>;
      return(
        <tr clothesTables__table_item>
          <td> {name} </td>
          <td> {this.props.product.price} </td>
        </tr>
      )
    }
  }
  
export default ClothesTable;