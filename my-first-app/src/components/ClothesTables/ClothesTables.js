import React, { Component } from 'react';
import ClothesTable from './ClothesTable'
import './style.scss'


const PRODUCTS1 = [
    {category: 'Sporting Goods', price: '$49.99', stocked: true, name: 'Football'},
    {category: 'Sporting Goods', price: '$9.99', stocked: false, name: 'Baseball'},
    {category: 'Sporting Goods', price: '$29.99', stocked: false, name: 'Basketball'},
    {category: 'Sporting Goods', price: '$29.99', stocked: true, name: 'Basetball'},
    {category: 'Electronics', price: '$99.99', stocked: true, name: 'iPod Touch'},
    {category: 'Electronics', price: '$399.99', stocked: false, name: 'iPhone 5'},
    {category: 'Electronics', price: '$199.99', stocked: true, name: 'Nexus 7'},
    {category: 'Electronics', price: '$199.99', stocked: false, name: 'iPhone 6'},
  ];

  const PRODUCTS2 = [
    {category: 'Sporting Goods', price: '$9.99', stocked: false, name: 'Ping-pong'},
    {category: 'Sporting Goods', price: '$19.99', stocked: true, name: 'Racing'},
    {category: 'Sporting Goods', price: '$39.99', stocked: false, name: 'Poker'},
    {category: 'Sporting Goods', price: '$129.99', stocked: false, name: 'Basetball'},
    {category: 'Phones', price: '$199.99', stocked: true, name: 'iPod Touch 4'},
    {category: 'Phones', price: '$2399.99', stocked: false, name: 'iPhone 5'},
    {category: 'Phones', price: '$4199.99', stocked: false, name: 'Nexus 15'},
    {category: 'Phones', price: '$6199.99', stocked: false, name: 'iPhone 16'},
  ];


class ClothesTables extends Component{
 
      
      render(){
        return(
          <div className='clothesTables'>
                <h1 className='h1'>ClothesTables</h1>
                <div className='clothesTables__items'>
                    <ClothesTable products={PRODUCTS1} title='First table'/>
                    <ClothesTable products={PRODUCTS2} title='Second table'/>
                </div>
          </div>
        )
      }
}

export default ClothesTables;