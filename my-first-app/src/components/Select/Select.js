import React, { Component } from 'react';

class Select extends Component {
    render(){
        return(
            <select onChange={this.onChangeSelect}>
                <option key='0' value='1'>2015</option>
                <option key='1' value='2'>2016</option>
            </select>
        )
    } 
}

export default Select;
