import React, { Component } from 'react';
import Cars from '../components/LearnReact/Car/Cars';
import CarsTemplate from '../components/LearnReact/Car/CarsTemplate'
class ComponentTest extends Component{
    goToHomeHandler = () => {
        this.props.history.push('/')
    }

    render(){
        return(
            <div className='componentTest'>
                <button className='' onClick={this.goToHomeHandler}>Go to home page</button>
                <Cars />
                <hr />
                <CarsTemplate />
            </div>
        )
    }
}

export default ComponentTest;