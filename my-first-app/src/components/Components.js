import React, { Component } from 'react';
import Chart from './Chart/Chart'
import DropdownItems from './Dropdown/DropdownItems';
import RegistrationForm from './RegistrationForm/RegistrationForm';
import Cars from './LearnReact/Car/Cars'
import TempCalculator from './TempCalculator/TempCalculator'
import ClothesTables from './ClothesTables/ClothesTables'

class Components extends Component{
    render(){
        return(
            <div className='components'>
                <Chart />
                <hr />
                <DropdownItems />
                <hr />
                <RegistrationForm />
                <hr />
                <Cars />
                <hr />
                <TempCalculator />
                <hr />
                <ClothesTables />
                <hr />
            </div>
        )
    }
}

export default Components;