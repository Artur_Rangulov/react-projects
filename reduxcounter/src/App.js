import React, { Component } from 'react';
import { connect } from 'react-redux'
import { add, sub, mult, addNumber, subNumber, asyncAdd  } from './redux/actions/actions';


class App extends Component {

  // state = {
  //   count: 0
  // }

  // clickHandler = (value) => {
  //   this.setState({
  //     count: value + this.state.count
  //   })
  // }

  render(){
    
    return (
      <div>
        <div>
          Count: {this.props.count}
        </div>
        <div>
          <button onClick={this.props.onAdd}>+3</button>
          <button onClick={this.props.onSub}>-3</button>
          <button onClick={() => this.props.onMult(2)}>*2</button>
        </div>
        <div>
          <button onClick={() => this.props.onAddNumber(15)}>+15</button>
          <button onClick={() => this.props.onSubNumber(-17 )}>-17</button>
        </div>
        <div>
          <button onClick={() => this.props.onAsyncAdd(100)} >
            Прибавить асинхронно 100
          </button>
        </div>

        
      </div>
    )}
}

//connect
//1. mapstatetoprops

function mapStateToProps (state) {
  return {
    count: state.counter1.count
  }
}

// 
function mapDispathToProps(dispatch){
  return {
    onAdd: () => dispatch(add()),
    onSub: () => dispatch(sub()),
    onMult: number => dispatch(mult(number)),
    onAddNumber: number => dispatch(addNumber(number)),
    onSubNumber: number => dispatch(subNumber(number)),
    onAsyncAdd: number => dispatch(asyncAdd(number))
  }
}

// если не хотим передавать state
// export default connect(null, mapDispathToProps)(App)

export default connect(mapStateToProps, mapDispathToProps)(App)