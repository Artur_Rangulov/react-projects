import {
    ADD,
    ADD2,
    SUB,
    ADD_NUMBER,
    SUB_NUMBER,
    SHOW_TITLE,
    MULT
} from './actionTypes'

export function add() { 
    return {
        type: ADD
    }
}

export function add2(number) { 
    return {
        type: ADD2,
        value: number
    }
}

export function mult(number) { 
    return {
        type: MULT,
        value: number
    }
}

export function sub() { 
    return {
        type: SUB
    }
}

export function addNumber(number) { 
    return {
        type: ADD_NUMBER,
        value: number
    }
}

export function subNumber(number) { 
    return {
        type: SUB_NUMBER,
        value: number
    }
}

export function showTitle() {
    return {
        type: SHOW_TITLE
    }
}

export function asyncAdd(number) {
    return (dispath) => {
        setTimeout( () => {
            dispath(addNumber(number))
        }, 2000)
    }
}