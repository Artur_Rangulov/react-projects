import {ADD2} from '../actions/actionTypes'

const initialState = {
    count: 200
}

export default function counter2 (state = initialState, action) {

  switch (action.type){
    case ADD2: 
		return {
            count: state.count + action.value
        }
	default:
		return state
  }

}