import {ADD, SUB, MULT, ADD_NUMBER, SUB_NUMBER} from '../actions/actionTypes'

const initialState = {
    count: 0
}

export default function counter1 (state = initialState, action) {

  switch (action.type){
    case ADD: 
			return {
				count: state.count + 1
			}
		case SUB:
			return {
				count: state.count - 1
			}
		case MULT:
			return {
				count: state.count * action.value
			}
		case ADD_NUMBER:
			return {
				count: state.count + action.value
			}
		case SUB_NUMBER:
				return {
					count: state.count + action.value
				}
		default:
			return state
  }

}