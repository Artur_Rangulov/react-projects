import {SHOW_TITLE} from '../actions/actionTypes'

const initialState = {
    show: false
}

export default function title(state = initialState, action){

    switch(action.type){
        case SHOW_TITLE:
            return {
                show: !state.show
            }
        default:
            return state
    }
}