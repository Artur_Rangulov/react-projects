import React from 'react'
import {connect} from 'react-redux'
import { showTitle } from '../../redux/actions/actions';

const Dropdown = props => {
        const text = (
            <>
                <h1>Приветствую!</h1>
                <p>и тут чуть-чуть текста добавлю</p>
            </>
        )

        return (
            <div>
                <button onClick={props.onShow}>
                    {props.show ? 'закрыть' : 'открыть'}
                </button>
                {props.show && text}
            </div>
        )
}

function mapStateToProps(state){
    return {
        show: state.title.show
    }
}

function mapDispathToProps(dispath){
    return {
        onShow: () => dispath(showTitle())
    }

}

export default connect(mapStateToProps,mapDispathToProps)(Dropdown)

