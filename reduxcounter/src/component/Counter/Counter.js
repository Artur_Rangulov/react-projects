import React from 'react'
import {connect} from 'react-redux'
import { add2 } from '../../redux/actions/actions';

const Counter = props => {
    return (
      <div style={{'margin': '20px', 'border': '1px solid grey', 'width': '200px'}}>
        <h1>
          Count: {props.countic}
        </h1>
        <div>
          <button onClick={()=>props.onChange(15)}>+15</button>
          <button onClick={()=>props.onChange(-15)}>-15</button>
        </div>
      </div>
    )
  }

  function mapStateToProps(state){
      return {
          countic: state.counter2.count
      }
  }

  function mapDispathToProps(dispath){
      return {
          onChange: number => dispath(add2(number))
      }
  }

  export default connect(mapStateToProps, mapDispathToProps)(Counter)