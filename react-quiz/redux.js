const redux = require ('redux')



const initialState = {
    counter: 0
}

const reducer = (state = initialState, action) => {

    if (action.type === 'ADD'){
        return {
            counter: state.counter + action.value
        }
    }

    return state
}

const store = redux.createStore(reducer)

store.subscribe(() => {
    console.log('subscribe ', store.getState())
})

const addCounter = {
    type: 'ADD',
    value: 5
};

store.dispatch(addCounter)
store.dispatch(addCounter)
store.dispatch(addCounter)


