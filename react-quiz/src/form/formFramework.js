export function createControl(config, validation) {
    return {
        ...config,
        validation: validation,
        valid: !validation,
        touched: false,
        value:''
    }
}

export function validate (value, validation = null){
    if(!validation){
        return true
    }

    let isValid = true;

    if (validation.required) {
        isValid = value.trim() !== '' && isValid
    }
    return isValid
    
}

export function validateFrom (fromControls){
    let isFormvalid = true;

    for (let control in fromControls){   //перебирает все поля и у прототипа тоже
        if(fromControls.hasOwnProperty(control)){   // смотрит только свойства принадлежащие самому объекту
            isFormvalid = fromControls[control].valid && isFormvalid
        }
    }
    return isFormvalid;
}