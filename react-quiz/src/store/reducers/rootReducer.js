import {combineReducers} from 'redux'
import quizReducer from './quiz'
import CreateReducer from './createQuiz';
import authReducer from './auth';

export default combineReducers({
    quiz: quizReducer,
    createQuiz: CreateReducer,
    auth: authReducer
})
