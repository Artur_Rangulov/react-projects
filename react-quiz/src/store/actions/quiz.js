import axios from 'axios'
import {
    FETCH_QUIZES_START,
    FETCH_QUIZES_SUCCESS,
    FETCH_QUIZES_ERROR,
    FETCH_QUIZE_SUCCESS,
    QUIZE_SET_STATE,
    FINISH_QUIZ,
    QUIZ_NEXT_QUESTION,
    QUIZ_RETRY
} from './actionsTypes'

export function fetchQuizes() {
    return async dispath => {
        try {
            dispath(fetchQuizesStart())
            const responce = await axios.get('https://react-quiz-55d15.firebaseio.com/quizes.json');

            const quizes = [];

            Object.keys(responce.data).forEach((key, index) => {
                quizes.push({
                    id: key,
                    name: `Тест №${index+1}`
                })
            })

            dispath(fetchQuizesSuccess(quizes))

        } catch (e){
            dispath(fetchQuizesError(e))
        }
    }
}

export function fetchQuizesStart (quizes) {
    return {
        type: FETCH_QUIZES_START,
    }
}

export function fetchQuizesSuccess (quizes) {
    return {
        type: FETCH_QUIZES_SUCCESS,
        quizes
    }
}

export function fetchQuizesError (e) {
    return {
        type: FETCH_QUIZES_ERROR,
        error: e
    }
}

export function fetchQuizById (quizId){
    return async dispath => {
        dispath(fetchQuizesStart())

        try{
            const responce = await axios.get(`https://react-quiz-55d15.firebaseio.com/quizes/${quizId}.json`)
            const quiz = responce.data; 

            dispath(fetchQuizeSuccess(quiz))
        
        }catch(e){
            dispath(fetchQuizesError(e))
        }

    }
}

export function fetchQuizeSuccess (quiz) {
    return {
        type: FETCH_QUIZE_SUCCESS,
        quiz
    }
}

export function quizSetState (answerState, results){
    return {
        type: QUIZE_SET_STATE,
        answerState,
        results
    }
}

export function finishQuiz () {
    return {
        type: FINISH_QUIZ
    }
}

export function quizNextQuestion (number) {
    return {
        type: QUIZ_NEXT_QUESTION,
        activeQuestion: number,
        answerState: null
    }
}

export function quizAnswerCLick (answerId) {
    return (dispath, getState) => {
        const state = getState().quiz

        if (state.answerState){
            const key = Object.keys(state.answerState)[0];
            if (state.answerState[key] === 'success'){
                return
            }
        }

        const question = state.quiz[state.activeQuestion]
        const results = state.results;

        if (question.rightAnswerId === answerId){
            if(!results[question.id]){
                results[question.id] = 'success'
            }

            dispath(quizSetState({[answerId]: 'success'}, results))

            const timeout = window.setTimeout( () => {

                if(state.quiz[state.activeQuestion+1]){
                    dispath(quizNextQuestion(this.state.activeQuestion + 1))
                }else{
                    dispath(finishQuiz())
                }

                window.clearTimeout(timeout)
            }, 1000)

        }else{
            results[question.id]='error';
            dispath(quizSetState({[answerId]: 'error'}, results))
        }
    }
}

export function retryQuiz() {
    return {
        type: QUIZ_RETRY
    }
}