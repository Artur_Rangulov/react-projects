import axios from 'axios'
import { AUTH_SUCCESS, AUTH_LOGOUT } from './actionsTypes';

export function auth (email, password, isLoggin){
    return async dispath => {
        const authData = {
            email,
            password,
            returnSecureToken: true
        }

        let url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyBxhlkJr1xYdmWrXzcBUi0ng_ld8XpDYJY'

        if (isLoggin) {
            url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyBxhlkJr1xYdmWrXzcBUi0ng_ld8XpDYJY'
        }

        const responce = await axios.post(url, authData)

        const data = responce.data

        console.log(data)

        const expirationDate = new Date (new Date().getTime() + data.expiresIn * 1000)

        localStorage.setItem('token', data.idToken)
        localStorage.setItem('userId', data.localId)
        localStorage.setItem('expirationDate', expirationDate)

        dispath(authSuccess(data.idToken))

        dispath(autoLogout(data.expiresIn))

        
    }
}

export function autoLogout (time) {
    return dispath => {
        setTimeout(() => {
            dispath(logout())
        }, time * 1000)
    }
}

export function logout () {
    localStorage.removeItem('token')
    localStorage.removeItem('userId')
    localStorage.removeItem('expirationDate')
    return {
        type: AUTH_LOGOUT
    }
}

export function authSuccess(token){
    return {
        type: AUTH_SUCCESS,
        token,
    }
}

export function autologin(){
    return async dispath => {
        const token = localStorage.getItem('token')
        console.log(token)
        if (!token){
            dispath(logout())
        }else{
            const expirationDate = new Date(localStorage.getItem('expirationDate'))
            if  (expirationDate <= new Date()) {
                dispath(logout())
            }else{
                dispath(authSuccess(token))
                dispath(autoLogout((expirationDate.getDate() - new Date().getDate()) / 1000))
            }
        }
    }
}