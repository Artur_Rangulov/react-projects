import axios from 'axios'
import { CREATE_QUIZ_QUESTION, RESET_QUIZ_CREATION } from './actionsTypes';

export function createQuizQuestion (item) {
    return {
        type: CREATE_QUIZ_QUESTION,
        item
    }
}

export function resetQuizCreation () {
    return {
        type: RESET_QUIZ_CREATION
    }
}

export function finishCreateQuiz() {
    return async (dispatch, getState) => {

        try {
            await axios.post('https://react-quiz-55d15.firebaseio.com/quizes.json', getState().createQuiz.quiz)
        } catch(e){
            console.log(e)
        }

        dispatch(resetQuizCreation())

    }
}