import React, {Component} from 'react'
import './Layout.scss'
import MenuToggle from '../../components/Navigation/MenuToggle/MenuToggle'
import Drawer from '../../components/Navigation/Drawer/Drawer'
import { connect } from 'react-redux';

class Layout extends Component{
    

    constructor(props){
        super(props);
        this.state = {
            menu: false
        }
    }

    toggleMenuHandler = () => {
        this.setState({
            menu: !this.state.menu
        }) 
    }

    menuCLoseHandler = () => {
        this.setState({
            menu: false
        }) 
    }

    render(){
        return(
            <div className='layout'>

                <Drawer 
                    isOpen = {this.state.menu}
                    onCLose = { this.menuCLoseHandler}
                    isAuthenticated = {this.props.isAuthenticated}
                />

                <MenuToggle
                    isOpen = {this.state.menu}
                    onToggle = {this.toggleMenuHandler}
                />

                <main className='layout__main'>
                    {this.props.children}
                </main> 
            </div>
        )
    }
}

function mapStateToProps( state ){
    return{
        isAuthenticated: !!state.auth.token
    }
}

export default connect(mapStateToProps)(Layout)