import React, { Component } from 'react';
import './Quiz.scss'
import ActiveQuiz from '../../components/ActiveQuiz/ActiveQuiz';
import FinishedQuiz from '../../components/FinishedQuiz/FinishedQuiz'
import Loader from '../../components/UI/Loader/Loader'
import {connect} from 'react-redux'
import {fetchQuizById, quizAnswerCLick, retryQuiz} from '../../store/actions/quiz'

class Quiz extends Component{

    componentDidMount(){
        this.props.fetchQuizById(this.props.match.params.id)
    }

    componentWillUnmount() {
        this.props.retryQuiz()
    }

    render(){
        return(
            
            <div className='quiz'>
                <div className='quiz__wrapper'>
                    <h1 className='h1'>Ответьте на все вопросы</h1>


                    {
                        this.props.loading || !this.props.quiz
                            ? <Loader />
                            : this.props.isFinished 
                                ? <FinishedQuiz 
                                    results={this.props.results}
                                    quiz={this.props.quiz}
                                    onRetry={this.props.retryQuiz}
                                />
                                : <ActiveQuiz 
                                    answers={this.props.quiz[this.props.activeQuestion].answers} 
                                    question={this.props.quiz[this.props.activeQuestion].question}
                                    onAnswerClick={this.props.quizAnswerCLick}
                                    quizLength={this.props.quiz.length}
                                    answerNumber={this.props.activeQuestion + 1}
                                    state={this.props.answerState}
                                    />
                    }

                </div>
            </div>
        )
    }
}

function mapStateToProps (state) {
    return {
        results: state.quiz.results,
        isFinished: state.quiz.isFinished,
        activeQuestion: state.quiz.activeQuestion,
        answerState: state.quiz.answerState,
        quiz: state.quiz.quiz,
        loading: state.quiz.loading
    }
}

function mapDispathToProps (dispath) {
    return {
        fetchQuizById: id => dispath(fetchQuizById(id)),
        quizAnswerCLick: answerId => dispath(quizAnswerCLick(answerId)),
        retryQuiz: () => dispath(retryQuiz())
    }
}

export default connect(mapStateToProps, mapDispathToProps)(Quiz)