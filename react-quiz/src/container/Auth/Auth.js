import React, {Component} from 'react'
import './Auth.scss'
import Button from '../../components/UI/Button/Button'
import Input from '../../components/UI/Input/Input'
import {connect} from 'react-redux'
import { auth } from '../../store/actions/auth';

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

class Auth extends Component{

    state = {
        isFormValid: false,
        formControls: {
            email: {
                value: '',
                type: 'email',
                label: 'Email',
                errorMessage: 'Введите корректный email',
                valid: false,
                touched: false,
                validation: {
                    required: true,
                    email: true
                }
            },
            password: {
                value: '',
                type: 'password',
                label: 'Пароль',
                errorMessage: 'Введите пароль больше 6-ти символов',
                valid: false,
                touched: false,
                validation: {
                    required: true,
                    minLength: 6
                }
            }
        }
    }

    loginHandler = () => {

        this.props.auth(
            this.state.formControls.email.value,
            this.state.formControls.password.value,
            true
        )

        
    }

    loginRegister = () => {

        this.props.auth(
            this.state.formControls.email.value,
            this.state.formControls.password.value,
            false
        )
    }

    sumbitHandler = (event) =>{
        event.preventDefault()
    }

    

    validateControl = (value, validation) => {
        if (!validation) {
            return true;
        }

        let isValid = true;

        if (validation.required){
            isValid = value.trim !== '' && isValid
        }

        if (validation.email){
            isValid = validateEmail(value) && isValid
        }

        if (validation.minLength){
            isValid = value.length >= validation.minLength  && isValid
        }

        return isValid;
    }

    onChangeHandler = (event, controlName) => {
      
        const formControls = { ...this.state.formControls};  //поверхностное копирование, только примитивов
        const control = { ...formControls[controlName] }; //поэтому необходимо сделать более глубокое копирование

        control.value = event.target.value;
        control.touched = true;
        control.valid = this.validateControl(control.value, control.validation)

        formControls[controlName] = control;

        let isFormValid = true;

        Object.keys(formControls).forEach( (name) => {
            isFormValid = formControls[name].valid && isFormValid
        })

        this.setState({
            formControls: formControls, // можно недублировать, если имена одинаковые
            isFormValid: isFormValid
        })
    }

    renderInputs() {
        return Object.keys(this.state.formControls).map( (controlName, index) => {
            const control = this.state.formControls[controlName]
            return (
                <Input
                    key={index}
                    type={control.type}
                    value={control.value}
                    valid={control.valid}
                    touched={control.touched}
                    errorMessage={control.errorMessage}
                    label={control.label}
                    shouldValidate={!!control.validation}
                    onChange={event => this.onChangeHandler(event, controlName)}

                />
            )
        })
    }

    render(){
        return(
            <div className='auth'>
                <div className='auth__container'>
                    <h1>Авторизация</h1>

                    <form className='auth__form' onSubmit={this.sumbitHandler}>
                        
                        {this.renderInputs()}

                        <Button type='btn-success' onClick={this.loginHandler} disabled={!this.state.isFormValid}>Войти</Button>
                        <Button type='btn-primary' onClick={this.loginRegister} disabled={!this.state.isFormValid}>Зарегистрироваться</Button>
                    </form>
                </div>
            </div>
        )
    }
}

function mapDispathToProps(dispath){
    return {
        auth: (email, password, isLogin) => dispath(auth(email, password, isLogin))
    }
}

export default connect(null, mapDispathToProps)(Auth)