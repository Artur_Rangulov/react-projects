import React, {Component} from 'react'
import {NavLink} from 'react-router-dom'
import './QuizList.scss'
import Loader from '../../components/UI/Loader/Loader'
import { connect } from 'react-redux';
import {fetchQuizes} from '../../store/actions/quiz'

class QuizList extends Component{
    
    renderQuizes(){
        return this.props.quizes.map( (quiz) => {
            return(
                <li className='quizList__item' key={quiz.id}>
                    <NavLink className='quizList__link' to={'/quiz/' + quiz.id} > {quiz.name}</NavLink>
                </li>
            )
        })
    }

    componentDidMount(){
        this.props.fetchQuizes()
    }

    render(){
        return(
            <div className='quizList'>
                    {this.props.loading && this.props.quizes.length !== 0
                        ? <Loader /> 
                         
                        : <div>
                            <h1 className='quizList__h1'>Список тестов</h1>
                                <ul className='quizList__list'>
                                    {this.renderQuizes()}
                                </ul>
                          </div>
                    }
                
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        quizes: state.quiz.quizes,
        loading: state.quiz.loading
    }
}

function mapDispathToProps(dispath){
    return {
        fetchQuizes: () => dispath(fetchQuizes())
    }
}

export default connect(mapStateToProps, mapDispathToProps)(QuizList)