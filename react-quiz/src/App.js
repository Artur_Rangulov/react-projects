import React, { Component } from 'react';
import Layout from './hoc/Layout/Layout'
import Quiz from './container/Quiz/Quiz'
import {Route, Switch, Redirect} from 'react-router-dom'
import QuizList from './container/QuizList/QuizList'
import Auth from './container/Auth/Auth'
import QuizCreator from './container/QuizCreator/QuizCreator'
import Error404 from './container/Error404/404'
import {connect} from 'react-redux'
import Logout from './components/Logout/Logout';
import { autologin } from './store/actions/auth';


class App extends Component {
  componentDidMount(){
    this.props.autoLogin()
  }

  render() {
    
    let routes = (
      <Switch>
        <Route path='/auth' component={Auth} />
        <Route path='/' exact component={QuizList} />
        <Route component={Error404} />
      </Switch>
    )

    if (this.props.isAuthenticated) {
      routes = (
        <Switch>
          <Route path='/quiz-creator' component={QuizCreator} />
          <Route path='/quiz/:id' component={Quiz} />
          <Route path='/logout' component={Logout} />
          <Route path='/' exact component={QuizList} />
          <Redirect to={'/'} />
        </Switch>
      )
    }
    
    return (
      
      <Layout>
        {routes}
      </Layout>
    );
  }
}

function mapStateToProps(state){
  return {
    isAuthenticated: !!state.auth.token
  }
}

function mapDispathToProps (dispath){
  return {
    autoLogin: () => dispath(autologin())
  }
}

export default connect(mapStateToProps, mapDispathToProps)(App);
