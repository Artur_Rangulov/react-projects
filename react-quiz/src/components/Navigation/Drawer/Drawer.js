import React, {Component} from 'react'
import './Drawer.scss'
import BackDrop from '../../UI/BackDrop/BackDrop' 
import {NavLink} from 'react-router-dom'



export default class Drawer extends Component{

    clickHandler = () => {
        this.props.onCLose()
    }

    renderLinks(links){
        return(
            links.map( (link, index) => {
                return(
                    <li className='nav__item' key={index}>
                        <NavLink 
                            to={link.to} 
                            exact={link.exact} 
                            className='nav__link'
                            activeClassName='nav__link-active'
                            onClick={this.clickHandler}
                        >
                                {link.label}
                        </NavLink>
                    </li>
                )
            })
            
        )
    }

    render(){

        const links = [
            {to: '/', label: 'Список', exact: true}
        ]

        if (this.props.isAuthenticated){
            links.push({to: '/quiz-creator', label: 'Создать тест', exact: false})
            links.push({to: '/logout', label: 'Выйти', exact: false})
        } else {
            links.push({to: '/auth', label: 'Авторизация', exact: false})
        }

        const cls = [
            'nav',
        ]
    
        if (!this.props.isOpen) {
            cls.push('nav-close')
        }
        return(
            <React.Fragment>
                <nav className={cls.join(' ')}>
                        <ul className='nav__list'>
                            { this.renderLinks(links) }
                        </ul>
                </nav>
                { (this.props.isOpen) ? <BackDrop onCLick={this.props.onCLose}/> : null }
            </React.Fragment>
        )
    }
}