import React from 'react'
import './Select.scss'
import PropTypes from 'prop-types'

const Select = props => {
    const htmlFor = `${props.label}-${Math.round(Math.random()*1000000)}`
    
    return(
        <div className='select'>
            <label htmlFor={htmlFor}>{props.label || 'Выберете необходимое'}</label>
            <select 
                id={htmlFor}
                value={props.value}
                onChange={props.onChange}
            >
                {props.options.map((option,index) => {
                    return (
                        <option key={option.value + index} value={option.value || index}>
                            {option.text || index}
                        </option>
                    )
                })}

            </select>
        </div>
    )
}

Select.propTypes = {
    label: PropTypes.string,
    value: PropTypes.number,
    onChange: PropTypes.func,
    options: PropTypes.array,
    text: PropTypes.number,
}

export default Select;