import React from 'react';
import PropTypes from 'prop-types';
import './Input.scss'


function isInvalid ({valid, touched, shouldValidate}) {
    return !valid && shouldValidate && touched
}

const Input = props => {

    const inputType = props.type || 'text';
    const inputClass = [
        'input',
        props.class
    ] ;
    const htmlFor = `${inputType}-${Math.round(Math.random()*1000000)}`

    if (isInvalid(props)) {
        inputClass.push('input-invalid')
    }

    return(
        <div className={inputClass.join(' ')}>
            <label htmlFor={htmlFor}>{props.label}</label>
            <input 
                type={inputType}
                id={htmlFor}
                value={props.value}
                onChange={props.onChange}
            />

            { isInvalid(props) ? <span>{props.errorMessage || 'Поле заполнено некорректно'}</span> : null}
            
        </div>
    )

};

Input.propTypes = {
    type: PropTypes.string,
    class: PropTypes.string,
    label: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func,
    errorMessage: PropTypes.string,
    shouldValidate: PropTypes.bool,
    touched: PropTypes.bool,
    vaild: PropTypes.bool
  };

export default Input;