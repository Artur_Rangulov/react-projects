import React from 'react';
import './AnswersItem.scss'


const AnswersItem = props => {

    const cls = ['answers__item']
    if (props.state){
        cls.push(props.state)
    }

    return(
        <li className={cls.join(' ')} onClick={() => props.onAnswerClick(props.answer.id)}>
            { props.answer.text }

        </li>
)}

export default AnswersItem