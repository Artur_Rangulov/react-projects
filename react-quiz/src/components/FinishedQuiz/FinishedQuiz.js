import React from 'react'
import './FinishedQuiz.scss'
import Button from '../UI/Button/Button'
import {Link} from 'react-router-dom'

const FinishedQuiz = props => {
    /*var successCount = 0;
    for (var res in props.results){
        if (props.results[res] === 'success'){
            successCount = successCount + 1;
        }
    }*/

    const successCount = Object.keys(props.results).reduce((total, key)=> {
        if (props.results[key] === 'success'){
            total++
        }
        return total
    }, 0)
     
    return(
        <div className='finishedQuiz'>
            <ul className='finishedQuiz__list'>

                {
                    props.quiz.map( (quizItem, index) => {
                        const cls = [
                            'fa',
                            props.results[quizItem.id] === 'error' ? 'fa-times fa-times-error' : 'fa-check fa-check-success',
                        ]

                        return(
                            <li key={index} className='finishedQuiz__item'>
                                <strong>{ index + 1 }.&nbsp;</strong>
                                {quizItem.question}
                                <i className={cls.join(' ')}/>
                            </li>
                        )
                    })
                }

            </ul>

            <p>Правильно {successCount} из {props.quiz.length}</p>

            <div className='finishedQuiz__repeat'>
                <Button onClick={props.onRetry} type='btn-primary'>Повторить</Button>
                <Link to={'/'}>
                    <Button type='btn-success' >Перейти в список тестов</Button>
                </Link>
            </div>
        </div>
    )
}

export default FinishedQuiz;